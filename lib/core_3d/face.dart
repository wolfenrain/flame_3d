part of core_3d;

class Face {
  Vertex vertex0;

  Vertex vertex1;

  Vertex vertex2;

  Vertex vertex3;

  Color color;

  List<Vertex> get vertices => [vertex0, vertex1, vertex2, vertex3];

  Face(this.vertex0, this.vertex1, this.vertex2, this.vertex3, this.color)
      : assert(
          vertex0 != null &&
              vertex1 != null &&
              vertex2 != null &&
              vertex3 != null,
        );
}
