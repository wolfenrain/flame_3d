part of core_3d;

class Vertex {
  double x, y, z;

  Vertex(this.x, this.y, this.z);
}
