import 'package:flutter/material.dart';
import 'package:flame_3d/core_3d/core_3d.dart';
import 'package:flame_3d/objects/obj.dart';

class Plane extends Obj {
  double x, y, z;

  double width, depth;

  Plane(this.x, this.y, this.z, this.width, this.depth) {
    vertices = [
      Vertex(x, y, z),
      Vertex(x, y + depth, z),
      Vertex(x + width, y, z),
      Vertex(x + width, y + depth, z),
    ];
    faces = [
      Face(vertices[0], vertices[1], vertices[2], vertices[3], Colors.green),
    ];
    edges = [
      Offset(0, 1),
      Offset(1, 0),
      Offset(1, 1),
    ];
    assertObj();
  }
}
