import 'package:flutter/material.dart';
import 'package:flame_3d/core_3d/core_3d.dart';

class Obj {
  List<Vertex> vertices;

  List<Offset> edges;

  List<Face> faces;

  /// Assertion to check if the object is constructed properly.
  ///
  /// For mroe information see: https://www.mathsisfun.com/geometry/vertices-faces-edges.html.
  @mustCallSuper
  assertObj() {
    assert(faces.length + vertices.length - edges.length == 2);
  }

  void wireframe(Canvas canvas) {
    for (var vertex in vertices) {
      canvas.drawCircle(
        Offset(vertex.x, vertex.y),
        4,
        Paint()..color = Colors.blue,
      );
    }

    for (var e = 0; e < edges.length; e++) {
      var n0 = edges[e].dx;
      var n1 = edges[e].dy;
      var vertex0 = vertices[n0.toInt()];
      var vertex1 = vertices[n1.toInt()];
      canvas.drawLine(
        Offset(vertex0.x, vertex0.y),
        Offset(vertex1.x, vertex1.y),
        Paint()..color = Colors.blueAccent,
      );
    }
  }
}
