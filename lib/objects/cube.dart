
import 'package:flutter/material.dart';
import 'package:flame_3d/core_3d/core_3d.dart';
import 'package:flame_3d/objects/obj.dart';

class Cube extends Obj {
  double x, y, z;

  double width, height, depth;

  Cube(this.x, this.y, this.z, this.width, this.height, this.depth) {
    vertices = [
      Vertex(x, y, z),
      Vertex(x, y, z + depth),
      Vertex(x, y + height, z),
      Vertex(x, y + height, z + depth),
      Vertex(x + width, y, z),
      Vertex(x + width, y, z + depth),
      Vertex(x + width, y + height, z),
      Vertex(x + width, y + height, z + depth),
    ];

    faces = [
      Face(vertices[0], vertices[1], vertices[2], vertices[3], Colors.green),
      Face(vertices[0], vertices[1], vertices[4], vertices[5], Colors.red),
      Face(vertices[4], vertices[5], vertices[6], vertices[7], Colors.cyan),
      Face(vertices[2], vertices[3], vertices[6], vertices[7], Colors.yellow),
      Face(vertices[0], vertices[2], vertices[4], vertices[6], Colors.pink),
      Face(vertices[1], vertices[3], vertices[5], vertices[7], Colors.white),
    ];

    edges = [
      Offset(0, 1),
      Offset(1, 3),
      Offset(3, 2),
      Offset(2, 0),
      Offset(4, 5),
      Offset(5, 7),
      Offset(7, 6),
      Offset(6, 4),
      Offset(0, 4),
      Offset(1, 5),
      Offset(2, 6),
      Offset(3, 7),
    ];
    assertObj();
  }
}