import 'dart:math';
import 'dart:ui';

import 'package:flame/flame.dart';
import 'package:flame/fps_counter.dart';
import 'package:flame/game.dart';
import 'package:flame/gestures.dart';
import 'package:flame/position.dart';
import 'package:flame/text_config.dart';
import 'package:flutter/material.dart';
import 'package:flame_3d/objects/cube.dart';
import 'package:flame_3d/objects/obj.dart';
import 'package:flame_3d/objects/plane.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Flame.util.setLandscape();
  await Flame.util.fullScreen();
  runApp(Game3D().widget);
}

class Game3D extends Game with FPSCounter, MultiTouchDragDetector {
  Size _size;

  List<Obj> objects;

  Game3D() {
    objects = [
      Plane(-200, -200, 0, 400, 400),
      Cube(-50, -50, -100, 100, 100, 100),
    ];
  }
  @override
  void resize(Size size) => _size = size;

  @override
  void render(Canvas canvas) {
    final vertexList = <Offset>[];
    final colorList = <Color>[];

    canvas.save();
    canvas.translate(_size.width / 2, _size.height / 2);

    objects.forEach((cube) {
      cube.faces.forEach((face) {
        vertexList.addAll([
          Offset(face.vertices[0].x, face.vertices[0].y),
          Offset(face.vertices[1].x, face.vertices[1].y),
          Offset(face.vertices[2].x, face.vertices[2].y),
          Offset(face.vertices[3].x, face.vertices[3].y),
          Offset(face.vertices[2].x, face.vertices[2].y),
          Offset(face.vertices[1].x, face.vertices[1].y),
        ]);
        colorList.addAll([
          face.color,
          face.color,
          face.color,
          face.color,
          face.color,
          face.color,
        ]);
      });
    });

    final vertices = Vertices(
      VertexMode.triangles,
      vertexList,
      colors: colorList,
    );

    canvas.drawVertices(
      vertices,
      BlendMode.src,
      Paint(),
    );

    if (recordFps()) {
      objects.forEach((cube) => cube.wireframe(canvas));
    }

    canvas.restore();
    if (recordFps()) {
      TextConfig(color: Colors.green).render(
        canvas,
        '${fps().toStringAsFixed(2)}',
        Position.empty(),
      );
    }
  }

  @override
  void update(double t) {}

  void rotateX3D(double theta) {
    var sinTheta = sin(theta);
    var cosTheta = cos(theta);

    for (var cube in objects) {
      for (var vertex in cube.vertices) {
        var y = vertex.y;
        var z = vertex.z;
        vertex.y = y * cosTheta - z * sinTheta;
        vertex.z = z * cosTheta + y * sinTheta;
      }
    }
  }

  void rotateY3D(double theta) {
    var sinTheta = sin(theta);
    var cosTheta = cos(theta);

    for (var cube in objects) {
      for (var vertex in cube.vertices) {
        var x = vertex.x;
        var z = vertex.z;
        vertex.x = x * cosTheta - z * sinTheta;
        vertex.z = z * cosTheta + x * sinTheta;
      }
    }
  }

  void rotateZ3D(double theta) {
    var sinTheta = sin(theta);
    var cosTheta = cos(theta);

    for (var cube in objects) {
      for (var vertex in cube.vertices) {
        var x = vertex.x;
        var y = vertex.y;
        vertex.x = x * cosTheta - y * sinTheta;
        vertex.y = y * cosTheta + x * sinTheta;
      }
    }
  }

  @override
  bool recordFps() => true;

  @override
  void onReceiveDrag(DragEvent drag) {
    Offset previousPosition = drag.initialPosition;
    drag.onUpdate = (DragUpdateDetails details) {
      var dx = (details.globalPosition.dx - previousPosition.dx) / 100;
      var dy = (details.globalPosition.dy - previousPosition.dy) / 100;

      previousPosition = details.globalPosition;

      rotateY3D(dx);
      rotateX3D(dy);
    };
  }
}
